#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../inc/timestamp_acquisition.h"

#define titre() system("cls");\
printf("Jours speciaux\n")
#define DEBUTJOURFERIE 4
#define titre2() titre();\
printf("jours feries")
#include <conio.h>

/** \exceptions
 * 0: début cours
 * 1: fin cours
 * 2: debut vacances
 * 3: fin vacances
 * +:jours sans cours
 */
//acquiert toutes les dates de cours/vacances/jours féries
int special_events_acquisition(int exceptions[])
{
    int choix=0;
    int i=DEBUTJOURFERIE;
    titre();
    printf("Veuillez rentrer la date de debut des cours sous la forme AAAAMMJJ : ");
        timestamp_acquisition(exceptions,0);
    titre();
    printf("\nDebut des cours : %d\nVeuillez rentrer la date de fin des cours sous la forme AAAAMMJJ : ",exceptions[0]);
        timestamp_acquisition(exceptions,1);
    titre();

    printf("\nVoulez-vous rajouter des dates de vacances pour ce semestre (O/N) ?");
        choix=getch();
    if(choix==111||choix==79)//si saisie = o ou O
    {
        printf("\nDebut des cours : %d\nFin des cours : %d\nVeuillez rentrer la date de debut de vacances sous la forme AAAAMMJJ : ",exceptions[0],exceptions[1]);
            timestamp_acquisition(exceptions,2);
        titre();
        printf("Debut des cours : %d\nFin des cours : %d\nDebut des vacances : %d\nVeuillez rentrer la date de fin de vacances sous la forme AAAAMMJJ : ",exceptions[0],exceptions[1],exceptions[2]);
            timestamp_acquisition(exceptions,3);
    }
    else
    {
        exceptions[2]=-1;//si jamais on veut pas mettre de dates de vacances
        exceptions[3]=-1;
    }

    titre2();

    printf("\nVoulez-vous rentrer un/des jours feries ? (O/N)");
    choix=0;
    choix=getch();
    fflush(stdin);
    if(choix==111||choix==79)//saisie premier jour ferié
    {

        printf("\nRentrez le jour ferie n %d (AAAAMMJJ):",i-3);
        timestamp_acquisition(exceptions,i);
        titre2();
        printf("\nVoulez-vous rajouter un autre jour ferie ? (O/N)");
        choix=getch();
        fflush(stdin);
        while(choix==111||choix==79)//boucle pour rentrer des jours fériés
        {
            i++;
            titre();
            printf("\nRentrez le jour ferie n %d:",i-3);
            timestamp_acquisition(exceptions,i);
            titre2();
            printf("\nContinuez ? (O/N)");
            choix=getch();
            fflush(stdin);
        }
    }

    system("cls");
    return i;
}
