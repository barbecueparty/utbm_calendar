#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../inc/inc_structure.h"
//transforme un AAAAMMJJ en AAAAMMJJTHHMMSS avec les bonnes informations
void make_full_timestamp(char destination[],cours courses[],int exceptions[],moment mom,int i)
{
    snprintf(destination,9,"%d",exceptions[0]);//print d'un int dans un string

    strcat(destination,"T");
    if(mom==START) strcat(destination,courses[i].startHour);//si g�n�ration date pour d�but du cours
    if(mom==END) strcat(destination,courses[i].endHour);
    strcat(destination,"00");

}//2019 01 01 T 05 05 00 Z
