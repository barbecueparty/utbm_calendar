#include <stdio.h>
#include <stdlib.h>

#define TAILLE_LIGNES 150

//réinitialise (re-remplit correctement selon le modele)le fichier raw input
void raw_input_reset(void)
{
    FILE* f_raw_input = NULL;
    FILE* modele = NULL;
    f_raw_input = fopen("files/raw_input.txt", "w+"); // ouverture raw_input en ecriture
    modele = fopen("files/files_modele.txt", "r"); //ouverture modele en lecture
    char line[TAILLE_LIGNES]; //chaine de caractère contenant temporairement une ligne du modèle

    if(f_raw_input==NULL)
    {
        printf("raw_input.txt non trouve\n");
    }
    else
    {
        if(modele==NULL)
        {
            printf("modele.txt non trouve\n");
            system("pause");//si le programme n'a pas modele, il ne peut pas continuer. EXIT_FAILURE !
            exit(EXIT_FAILURE);
        }
        else
        {
            while(fgets(line, TAILLE_LIGNES, modele)!=NULL) // stocke les lignes de modele temporairement dans line tant qu'il y a des lignes dans le f_raw_input
            {
                fprintf(f_raw_input, "%s", line); //ecrase le texte de raw_input et ecrit dans raw_input la ligne du modele
            }
            fclose(f_raw_input);
            fclose(modele);
        }
    }
}
