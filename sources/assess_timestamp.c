#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

/** \type de timestamp
 * -1: non testé
 * 0 : refusé longueur interdite
 * 3 : short timestamp AAAAMMJJ
 * 4 : incohérence de date : avant date de début
 * 5 : incohérence de date : pas dans temps de cours
 * 6 : incohérence de date : date mal écrite
 */
 //fonction qui vérifie l'entrée de l'utilisateur et donne une valeur selon la validité de la saisie
int assess_timestamp(char timestampC[],int n,int exceptions[])
{
    int valid=-1, length=-1;
    length=strlen(timestampC);

    {//vérif du char, si bien nombre, bonne longueur
        if(length==8)
            {
                valid=3;
                for(int i=0;i<9;i++)
                {
                    if((timestampC[0]<48 && timestampC[i]>57)) valid=6;
                }
            }
        else valid=0;
    }

    long timestampI=atoi(timestampC);
    if(length==8)
    {
        switch (n)//vérif cohérence date
            {
            case 1://fin vacances
                {
                    if((exceptions[0]>timestampI)) valid=4;
                    break;
                }
            case 2://début vacances
                {
                    if((exceptions[0]>timestampI)||(exceptions[1]<timestampI)) valid=5;
                    break;
                }
            case 3://fin vacances
                {
                    if((timestampI<exceptions[2])) valid=4;
                    if((exceptions[0]>timestampI)||(exceptions[1]<timestampI)) valid=5;
                    break;
                }
            default://jour férié
                {
                    if(((exceptions[0]>timestampI)||(exceptions[1]<timestampI))&&n!=0) valid=5;
                    break;
                }
            }
        int P[8];
        for(int i=7;i>=0;i--)
        {
            P[i]=timestampI%10;
            timestampI/=10;
        }
        if(P[4]>1||(P[4]==1&&P[5]>2)||(P[6]==0&&P[7]==0)||(P[6]>3)||(P[7]>2&&P[6]>2)) valid=6;//vérifie l'écriture de la date (pas de mois 13)
    }
    return valid;
}
