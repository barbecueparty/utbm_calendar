#include <stdio.h>
#include <stdlib.h>
#include <math.h>

//fonction qui decompose un long yyyymmdd de 8 chiffres en un tableau de 8 chiffres dans l'ordre ddmmyyyy
void decompose_date(long nb, int nb_yyyymmdd[8])
{
    for(int i=0; i<=7; i++)
    {
        nb_yyyymmdd[i] = nb%10;
        nb = (nb-nb_yyyymmdd[i])/10;
    }
}
