#ifndef STRUCT_FILL_H_INCLUDED
#define STRUCT_FILL_H_INCLUDED

#include "inc_structure.h"

#define NOMBRE_COURS 18
#define TAILLE_LIGNES 150

void struct_fill(short int,short int , cours [NOMBRE_COURS], char[][TAILLE_LIGNES]);

#endif // STRUCT_FILL_H_INCLUDED
