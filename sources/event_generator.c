#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../inc/inc_structure.h"
#include "../inc/timestamp_now.h"
#include "../inc/make_full_timestamp.h"
#include "../inc/decompose_date.h"
#include "../inc/timestamp_range_generator.h"

#define TAILLE_EXDATE 200
#define DEBUTJOURFERIE 4

//g�n�re le fichier ICS avec toutes les informations acquises
void event_generator(FILE* ICS,cours courses[],int exceptions[])
{
    char now[sizeof("AAAAMMJJTHHMMSSZ")]="";
    int i=0;

    while(strcmp("",courses[i].day)!=0)
    {
        //BEGIN
            fprintf(ICS,"BEGIN:VEVENT\n");


        //DTSTART
        char start[sizeof("AAAAMMJJTHHMMSS")]="";
        make_full_timestamp(start,courses,exceptions,START,i);
        fprintf(ICS,"DTSTART;TZID=Europe/Paris:%s\n",start);


        //DTEND
        char end[sizeof("AAAAMMJJTHHMMSS")]="";
        make_full_timestamp(end,courses,exceptions,END,i);
        fprintf(ICS,"DTEND;TZID=Europe/Paris:%s\n",end);


        //RRULE
            if(*courses[i].frequency=='1')
            {
                fprintf(ICS,"RRULE:FREQ=WEEKLY;UNTIL=%d;BYDAY=%s\n",exceptions[1],courses[i].day);
            }
            else if(*courses[i].frequency=='2')
            {
                fprintf(ICS,"RRULE:FREQ=WEEKLY;UNTIL=%d;INTERVAL=2;BYDAY=%s\n",exceptions[1],courses[i].day);//RRULE
            }
            else
            {
                printf("\nErreur frequence !\n");
            }

        //EXDATE
                int j=DEBUTJOURFERIE;
                while(exceptions[j]!=-1)
                {
                    char temp[sizeof("AAAAMMJJTHHMMSSS")]="";
                        snprintf(temp,9,"%d",exceptions[j]);
                        strcat(temp,"T");
                        strcat(temp,courses[i].startHour);
                        strcat(temp,"00");
                    fprintf(ICS,"EXDATE;TZID=Europe/Paris:%s\n",temp);
                        j++;
                }
                if(exceptions[2]!=-1)
                {
                    long EXDATE[TAILLE_EXDATE];
                    int k=0;
                    timestamp_range_generator(EXDATE,exceptions[2],exceptions[3]);
                    while(EXDATE[k]!=-1)
                    {
                        fprintf(ICS,"EXDATE;TZID=Europe/Paris:%ldT%s00\n",EXDATE[k],courses[i].startHour);
                        k++;
                    }
                }


        //DTSTAMP UID DESCRIPTION LOCATION STATUS SUMMARY TRANSP END
            timestamp_now(now);
            fprintf(ICS,"DTSTAMP:%s\n",now);
            fprintf(ICS,"UID:%d@utbm.fr\n",i);
            fprintf(ICS,"DESCRIPTION:\n");
            fprintf(ICS,"LOCATION:%s\n",courses[i].room);
            fprintf(ICS,"STATUS:CONFIRMED\n");
            fprintf(ICS,"SUMMARY:%s %s\n",courses[i].UV,courses[i].group);
            fprintf(ICS,"TRANSP:OPAQUE\n");
            fprintf(ICS,"END:VEVENT\n\n");


        i++;
    }
    fprintf(ICS,"END:VCALENDAR");
}
