# UTBM CALENDAR

Lien gitlab : https://framagit.org/barbecueparty/utbm_calendar

Ce programme permet de générer un fichier ICS avec votre emploi du temps UTBM. Un fichier ICS est un fichier
qui détient toutes les informations de votre emploi du temps. Ce fichier est ensuite importable dans des
applications de calendrier comme Google Calendar ou iCal. L'endroit où importer le fichier dans ces
applications se trouve probablement dans les paramètres du calendrier (importer/exporter).

L'intérêt de ce programme est qu'il vous permet de :
* ne pas avoir à rentrer manuellement vos cours,
* ne pas avoir de cours créés sur une période de vacances ou de jour(s) férié(s).

Et il fonctionne pour tous les semestres !

DISCLAIM : Fonctionne uniquement sous windows


# COPYRIGHT & LICENSE

Copyright (c) 2019 Alexandre DESBOS, Flavien VARGUES, Mathis FERON

License : GNU General Public License v3.0.

See: https://choosealicense.com/licenses/gpl-3.0/
